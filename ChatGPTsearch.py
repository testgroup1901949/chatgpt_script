import undetected_chromedriver as uc
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
import os
import time
import json


##file_path = 'E:\\ChatgptApi\\crawl\\EP207.json'
##with open(file_path, 'r', encoding='utf-8') as file:
    ##file_content = file.read()
def login():
    chrome_driver_path = r"E:\\ChatgptApi\\crawl\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe"

    user_email = "xxxxxx@gmail.com"
    password = "xxxxxx"
    try:
        os.system('taskkill /im chromedriver.exe /F') # 目的是为了确保所有的进程都被关闭了
        os.system('taskkill /im chrome.exe /F') # 目的是为了确保所有的进程都被关闭了
    except OSError:
        pass
    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')
    options.add_argument("--start-maximized")
    driver = uc.Chrome(options=options,driver_executable_path=chrome_driver_path)
    driver.get('https://chat.openai.com/chat')
    time.sleep(1)

    try:


        # 点击登录按钮
        login_button = driver.find_element(By.XPATH,'//div[contains(@class,"mt-5")]/div/button[1]')
        time.sleep(1) # 这可以让我们看起来更像一只恐怖直立猿
        login_button.click()
        time.sleep(5) # 这可以让我们看起来更像一只恐怖直立猿


        # 选中帐号输入框
        user_email_input = driver.find_element(By.XPATH,'//*[@id="email-input"]')
        user_email_input.click() #先点击输入文本的输入框
        user_email_input.clear()
        user_email_input.send_keys(user_email)
        time.sleep(1) # 这可以让我们看起来更像一只恐怖直立猿

        # 获取下一步按钮
        login_next_step_button = driver.find_element(By.XPATH,'//*[@id="root"]/div/main/section/div[2]/button')
        login_next_step_button.click()
        time.sleep(1) # 这可以让我们看起来更像一只恐怖直立猿

        # 选中密码输入框
        user_password_input = driver.find_element(By.XPATH,'/html/body/div[1]/main/section/div/div/div/form/div[1]/div/div[2]/div/input')
        user_password_input.click() 
        user_password_input.clear()
        user_password_input.send_keys(password)
        time.sleep(2) 

        submit_button = driver.find_element(By.XPATH,'/html/body/div[1]/main/section/div/div/div/form/div[2]/button')
        submit_button.click()
        time.sleep(6)

        switch_button = driver.find_element(By.CSS_SELECTOR, "div[type='button'][aria-haspopup='menu']")
        switch_button.click()
        time.sleep(4)

        Chatgpt4_button = driver.find_element(By.CSS_SELECTOR, "div[class='flex items-center gap-3']")
        Chatgpt4_button.click()
        time.sleep(4)
#----------------customerGPT position
        customer = driver.find_element(By.XPATH, "//*[@id='__next']/div[1]/div[1]/div/div/div/div/nav/div[2]/div[2]/div[1]/a")
        customer.click()
        time.sleep(4)
    except Exception as e:
        print("here are some errors happened:",e)
    return driver 


def chat_start(driver):
    input_dir = 'E://xxxxxxxx//input'
    output_dir = 'E://xxxxxxxx//output'
    current_idx = 3 # 用于选择对应每个问题爬取得到的结果

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for filename in os.listdir(input_dir):

        driver.refresh()
        time.sleep(2)

        input_filepath = os.path.join(input_dir, filename)
        output_filepath = os.path.join(output_dir, os.path.splitext(filename)[0] + '.json')

        time.sleep(2)
        input_xpath = "//input[@type='file']"
        file_input_element = driver.find_element(By.XPATH, input_xpath)

        file_input_element.send_keys(input_filepath)
        time.sleep(5)
        question = "Help me summarize"
        question_input_area = driver.find_element(By.XPATH,'//*[@id="prompt-textarea"]')
        question_input_area.click() 
        question_input_area.clear()
        question_input_area.send_keys(question)
        time.sleep(1)

        # 点击发送问题
        submit_question_button = driver.find_element(By.XPATH,'//*[@data-testid="send-button"]')
        if submit_question_button:
            submit_question_button.click()
        
        print("*"*100)
        print("please wait for the answer")
        print("*"*100)  
        try:

            time.sleep(20)
            is_ok = driver.find_element(By.XPATH,'//*[@data-testid="send-button"]')

            answer = driver.find_element(by=By.XPATH, value='/html/body/div[1]/div[1]/div[2]/main/div[2]/div[1]/div/div/div/div[{}]/div/div/div[2]/div[2]/div[1]/div/div'.format(current_idx)).text
        except Exception as e:
            print("*"*100)
            print("please wait for another 15 second")
            print("*"*100)
            time.sleep(15)
            answer = driver.find_element(by=By.XPATH, value='/html/body/div[1]/div[1]/div[2]/main/div[2]/div[1]/div/div/div/div[{}]/div/div/div[2]/div[2]/div[1]/div/div'.format(current_idx)).text

        current_idx+=2
        print("*"*100)
        print("According to the question:{} Here is the answer".format(question))
        print("*"*100)
        with open(output_filepath, 'w', encoding='utf-8') as file:
            json.dump({"answer": answer}, file, ensure_ascii=False, indent=4)
        
        print(answer)

        print("*"*100) 


def start_server():
    print("start_server is going to run!")
    driver = login()
    chat_start(driver)


if __name__ == '__main__':
    start_server()